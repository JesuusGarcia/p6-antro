/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package antro.p6;



    public class Nodo<E> {
    private Nodo<E> izquierdo;
    private E elemento;
    private Nodo<E> Derecho;

    public Nodo(E elemento) {
        this.elemento = elemento;
    }

    public Nodo(Nodo<E> izquierdo, E elemento, Nodo<E> Derecho) {
        this.izquierdo = izquierdo;
        this.elemento = elemento;
        this.Derecho = Derecho;
    }

    public Nodo<E> getDerecho() {
        return Derecho;
    }

    public void setDerecho(Nodo<E> Derecho) {
        this.Derecho = Derecho;
    }

    public Nodo<E> getIzquierdo() {
        return izquierdo;
    }

    public void setIzquierdo(Nodo<E> izquierdo) {
        this.izquierdo = izquierdo;
    }

    public E getElemento() {
        return elemento;
    }

    public void setElemento(E elemento) {
        this.elemento = elemento;
    }

    @Override
    public String toString() {
        return elemento.toString();
    }
    
}
