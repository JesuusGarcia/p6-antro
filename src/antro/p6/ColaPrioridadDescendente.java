package antro.p6;

/*
* Autores: Brian Gutierrez y Jesus Garcia
*/
public class ColaPrioridadDescendente<E extends Comparable<E>> extends ColaDinamica<E> {
/*
    La clase ColaPrioridadDescendente hereda de la clase ColaDinamica y tambien tiene que sobreescribir los metodos que estan en
    la clase Cola
    
    */
 
    /*
    El metodo remover se sobreescribe y el metodo sirve para quitar personas o elementos
    de la cola segun su prioridad en este caso nos sirve para quitar personas de la cola
    y que entren al antro
    */
    @Override
    public E remover() {   
        E aux = elementos.get(0);
        for(int i = 0; i< elementos.tamanio() ; i++){
            if(aux.compareTo(elementos.get(i))<0)
                aux = elementos.get(i);    
            }               
        return  elementos.eliminar(elementos.indiceDe(aux))  ;
        
    }
/*
    El metodo toString se sobreescribe y usamos el toString ya definido en la clase ListaDoblementeLigada
    */
    @Override
    public String toString() {
       return elementos.toString(); 
    }
    
    /*
    El metodo insertar se sobreescribe y usamos el metodo agregar de ListaDoblementeLigada
    */
    
      @Override
    public void insertar(E elemento) {
        elementos.agregar(elemento);
    }
   }

    

