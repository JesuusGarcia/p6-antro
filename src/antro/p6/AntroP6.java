
package antro.p6;
import java.util.Random;

/*
* Autores: Brian Gutierrez y Jesus Garcia
*/

public class AntroP6 { 
    public static void main(String[] args) {
                     
       ColaPrioridadDescendente fila = new ColaPrioridadDescendente();
       
        Random random = new Random();         
        
        /*
        *Creamos noombres aleatorios para crear personas aleatorias en el for
        */
        
         String [] niñas ={"Ana", "Estela", "Mariana", "Paty",
         "Jimena","Ana","Carolina", "Andrea","Valeria","Anny" }; 
         
        String [] niños ={"Pedro",  "Yisus","Brian","Alexis","Federico", 
            "Kevin","Alex","Alejandro", "Jair","Mario"};
        
        double intervalo= (Math.random()*10)+10;
        
        /*
        * For que nos genera las personas aleatorias basadas en nuestro String de miños y niñas
        ademas de definirles el sexo (en base a niño y niña) y dinero aleatorio en un rango de 0.00
        hasta 5000.00
        */
        
        for(int i =0; i<= intervalo; i++){
            if(.5<Math.random()){
                int nombree = random.nextInt(10);
                Persona personito = new Persona(niños[nombree],'H',Math.random()*5000+1);
                fila.insertar(personito);
            }else{
               int nombree = random.nextInt(10);
               Persona personita = new Persona(niñas[nombree],'M',Math.random()*5000+1);
               fila.insertar(personita);
            }
        }
        
        
        System.out.println(fila);
        
        /*
        *For que nos quita o remueve a las personas que ya entraron al antro y en el orden 
        que entraron
        */
        for(int j=0; j <= intervalo; j++){
            
        System.out.println( "La persona entro al antro"+ ": " + fila.remover());
        } 
        
    }
}  