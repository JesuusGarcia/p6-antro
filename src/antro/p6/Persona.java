
package antro.p6;

/*
* Autores: Brian Gutierrez y Jesus Garcia
*/

public class Persona implements Comparable<Persona>  {   
    /*
    Creamos la clase persona en la que el posible comprar personas segun sus atributos
    */
    double dinero; 
    char sexo; 
    String nombre; 
    
/*
    Una persona tiene nombre, sexo y dinero
    */
    public Persona(String nombre, char sexo, double dinero) {       
        this.dinero = dinero;
        this.sexo = sexo;
        this.nombre = nombre; 
    }

    public double getDinero() {
        return dinero;
    }

    public String getNombre() {
        return nombre;
    }

    public char getSexo() {
        return sexo;
    }
    
   
    
    public void setDinero(double dinero) {
        this.dinero = dinero;
    }

    public void setSexo(char sexo) {
        this.sexo = sexo;
    }

    public void setNombre(String nombre) {
        
        this.nombre = nombre;
    }
    /*
    El metodo compareTo nos da el criterio para comparar personas 
    en el que podemos comparar hombres y mujeres, tambien se pueden comparar del mismo sexo
    */
    @Override
     public int compareTo(Persona otraPersona){
        if(this.sexo == 'M' && otraPersona.getSexo() != 'M')
            return 1;
        else if(this.sexo != 'M' && otraPersona.getSexo() == 'M')
            return -1;
        else if(this.dinero < otraPersona.dinero)
                return -1;
            else if(this.dinero > otraPersona.dinero)
                return 1;
        else
                return 0;  
    }
/*
     Para imprimir una persona se mostrara el nombre, sexo y el dinero que lleva
     */
    @Override
    public String toString (){ 
     return getNombre()+ "  " + getSexo()+ "  " + getDinero() ; 
       
    }
        
}
