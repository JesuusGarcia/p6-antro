package antro.p6;


public interface Lista<E> {
    void agregar(E elemento);
    void agregar(int indice, E elemento);
    E eliminar(int indice);
    void eliminar(E elemento);
    int indiceDe(E elemento);
    
    
    
    E get(int indice);
    boolean isVacia();
    int tamanio();
}
