
package antro.p6;
    
    public class ColaDinamica<E> implements Cola<E> {
    public Lista<E> elementos;

    public ColaDinamica() {
        this.elementos = new ListaDoblementeLigada<>();
    }
    
    @Override
    public void insertar(E elemento) {
        elementos.agregar(elemento);
    }

    @Override
    public E remover() {
        return elementos.eliminar(0);
    }
    
//      public Lista<E> getElementos() {
//        return elementos;
//    }
    
//      public void setElementos(Lista<E> elementos) {
//        this.elementos = elementos;
//    } 

    @Override
    public String toString() {
        return elementos.toString();
    }
    }

    

